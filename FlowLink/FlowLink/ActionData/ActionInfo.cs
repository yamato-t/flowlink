﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;


namespace FlowLink {

    // ------------------------------------------------------------------------------------------------
    /**
     * 
     */
    public class ActionInfo : Info {

        public enum ACTION_DATA {
            // Base
            SERIAL_NO   = Info.SELF_DATA.SERIAL_NO,
            TYPE        = Info.SELF_DATA.TYPE,

            // ActionInfo
            MOTION,
            FLAGS,
            NUM,
        };

        private string  m_strMotion = "none";
        private int     m_iFlags    = 0;


        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        [Category("2-アクション専用情報")]
        public string Motion {
            set { m_strMotion = value; }
            get { return m_strMotion; }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        [Category("2-アクション専用情報")]
        public int Flags {
            set { m_iFlags = value; }
            get { return m_iFlags; }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public ActionInfo() {
            m_strLabel      = "ACTION_INFO";
            m_eType         = TYPE.ACTION;
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public override bool InputLinks(string[] xParams) {

            if (0 == xParams.Count()) { return false; }

            int iLinkNum = xParams.Count() / (int)LINK_INFO.NUM;

            for (int iCurrent = 0; iCurrent < iLinkNum; iCurrent++) {

                int iOffset = iCurrent * (int)LINK_INFO.NUM;
                int iLinkSerialNo   = int.Parse(xParams[iOffset + (int)LINK_INFO.LINK_SERIAL_NO]);
                int iCondition      = int.Parse(xParams[iOffset + (int)LINK_INFO.CONDITION]);

                AddLink();
                LinkInfo[(LinkInfo.Count() - 1)].LinkSerialNo   = iLinkSerialNo;
                LinkInfo[(LinkInfo.Count() - 1)].Condition      = iCondition;
            }

            return true;
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public override void OutputLinks(StreamWriter xWriter) {
            string xStringLine = m_iSerialNo.ToString() + ",";

            if (null != m_xLinkInfo) { 
                foreach (LinkInfo xLink in m_xLinkInfo) {
                    xStringLine += xLink.LinkSerialNo.ToString()    + ",";
                    xStringLine += xLink.Condition.ToString()       + ",";
                }
            }

            xWriter.WriteLine(xStringLine);
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public override void OutputBinaryLinks(BinaryWriter xWriter) {
            // シリアル番号
            xWriter.Write(m_iSerialNo);

            // リンク数
            int linkNum = m_xLinkInfo == null ? 0 : m_xLinkInfo.Count();
            xWriter.Write(linkNum);
            
            // 各情報
            if (null != m_xLinkInfo) { 
                foreach (LinkInfo xLink in m_xLinkInfo) {
                    xWriter.Write(xLink.LinkSerialNo);
                    xWriter.Write(xLink.Condition);
                }
            }
        }

    }
}
