﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;


namespace FlowLink {

    // ------------------------------------------------------------------------------------------------
    /**
     * 
     */
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class LinkInfo {
        protected int m_iLinkSerialNo   = 0;
        protected int m_iCondition      = 0;

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public int LinkSerialNo {
            set { m_iLinkSerialNo = value; }
            get { return m_iLinkSerialNo; }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public int Condition {
            set { m_iCondition = value; }
            get { return m_iCondition; }
        }

    }


    // ------------------------------------------------------------------------------------------------
    /**
     * 
     */
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public abstract　class Info {

        public enum SELF_DATA {
            SERIAL_NO,
            TYPE,
            LABEL,
            NUM,
        };

        public enum TYPE {
            ACTION,
            CONDITION,
            FLOW,
            NUM,
        };

        protected enum LINK_INFO {
            LINK_SERIAL_NO,
            CONDITION,
            NUM,
        };

        protected string        m_strLabel      = "INFO";
        protected int           m_iSerialNo     = 0;
        protected TYPE          m_eType;
        protected LinkInfo[]    m_xLinkInfo;

        public abstract bool InputLinks(string[] xParams);
        public abstract void OutputLinks(StreamWriter xWriter);
        public abstract void OutputBinaryLinks(BinaryWriter xWriter);


        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public void InputSelf(string[] xParams) {
            if (0 == xParams.Count()) { return; }

            // シリアル番号
            int iSerialNo   = int.Parse(xParams[(int)SELF_DATA.SERIAL_NO]);
            // シリアル番号は作られた時に入力されている、差異があるとデータがおかしい
            if (iSerialNo != SerialNo) {
                string strMessage = "データのシリアル番号がおかしい シリアル番号[ " + iSerialNo.ToString() + " ]";
                MessageBox.Show(strMessage, "警告", MessageBoxButtons.OK);
            }
            SerialNo = iSerialNo;

            // タイプ
            int iType   = int.Parse(xParams[(int)SELF_DATA.TYPE]);
            m_eType     = (TYPE)Enum.Parse(typeof(TYPE), iType.ToString());

            // ラベル
            string strLabel = xParams[(int)SELF_DATA.LABEL];
            m_strLabel      = strLabel;
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public void OutputSelf(StreamWriter xWriter)
        {
            string  xStringLine     = m_iSerialNo.ToString() + ",";
            xStringLine             += ((int)m_eType).ToString() + ",";
            xStringLine             += m_strLabel + ",";

            xWriter.WriteLine(xStringLine);
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public void OutputBinarySelf(BinaryWriter xWriter) {

            int strLabelCount = m_strLabel.Count();

            int dataSize =  System.Runtime.InteropServices.Marshal.SizeOf(m_iSerialNo)  +
                            System.Runtime.InteropServices.Marshal.SizeOf((int)m_eType) +
                            System.Runtime.InteropServices.Marshal.SizeOf(strLabelCount)+
                            m_strLabel.Count();

            byte[] data = new byte[dataSize];


            //xWriter.Write(m_iSerialNo);            
            //xWriter.Write((int)m_eType);

            //xWriter.Write(m_strLabel.Count());
            //xWriter.Write(m_strLabel);
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public virtual void Reset() {
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public virtual void AddLink() {
            if(null == m_xLinkInfo) {
                m_xLinkInfo = new LinkInfo[1];
            } else {
                Array.Resize(ref m_xLinkInfo, (m_xLinkInfo.Count() + 1));
            }

            m_xLinkInfo[m_xLinkInfo.Count() - 1] = new LinkInfo();
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public virtual void RemoveLink(int iNo = -1) {

            if(-1 != iNo) {
                for(int i = iNo; i < (m_xLinkInfo.Count()-1); i++) {
                    m_xLinkInfo[i] = m_xLinkInfo[i+1];
                }
                Array.Resize(ref m_xLinkInfo, (m_xLinkInfo.Count() - 1));
            } else {
                Array.Resize(ref m_xLinkInfo, 0);
            }
        }


        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        [Category("1-情報")]
        public string Label {
            set { m_strLabel = value; }
            get { return m_strLabel; }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        [Category("1-情報")]
        [ReadOnlyAttribute(true)]
        public int SerialNo {
            set { m_iSerialNo = value; }
            get { return m_iSerialNo; }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        [Category("-リンク先情報")]
        public LinkInfo[]  LinkInfo {
            set { m_xLinkInfo = value; }
            get { return m_xLinkInfo; }
        }
    }
}
