﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace FlowLink {

    // ------------------------------------------------------------------------------------------------
    /**
     * 
     */
    public partial class PanelBase : UserControl {

        protected enum OPERTE_STATE {
            NONE,
            PUSH,
            HOLD,
        };

        protected Form1                 m_xParentForm1  = null;
        protected Info                  m_xInfo         = null;
        protected ArrayList             m_xLinkPanels   = new ArrayList();

        protected Point         m_vPreviousPos  = new Point();
        protected OPERTE_STATE  m_eOperateState = OPERTE_STATE.NONE;



        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public Info Info {
            set { m_xInfo = value; }
            get { return m_xInfo; }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public System.Windows.Forms.Panel LinkRect {
            get { return panel2; }
        }


        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public virtual void CreateInfo() {
            // サブクラス実装
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 自分の情報を入力
         */
        public virtual void InputSelf(string[] xParams) {
            m_xInfo.InputSelf(xParams);
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * リンク情報を入力
         */
        public virtual void InputLinks(string[] xParams) {
            bool bEnableLink = m_xInfo.InputLinks(xParams);

            if (bEnableLink) {
                LinkRect.BackColor = Color.LightBlue;
            }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 自分の情報を出力
         */
        public virtual void OutputSelf(StreamWriter xWriter) {
            m_xInfo.OutputSelf(xWriter);
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 自分の情報を出力
         */
        public virtual void OutputBinarySelf(BinaryWriter xWriter) {
            m_xInfo.OutputBinarySelf(xWriter);
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * リンク情報を出力
         */
        public virtual void OutputLinks(StreamWriter xWriter) {
            m_xInfo.OutputLinks(xWriter);
        }
        
        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public PanelBase() {
            InitializeComponent();

            CreateInfo();
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public Form1 ParentForm1 {
            set { m_xParentForm1 = value; }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public void SetLinkPanel(PanelBase xNextPanel) {

            bool bIsAlreadyLink = false;  

            if(null != Info.LinkInfo) {
                foreach(LinkInfo xLink in Info.LinkInfo) {
                    if(xLink.LinkSerialNo == xNextPanel.Info.SerialNo) { bIsAlreadyLink = true; break;} 
                }
            }

            if(!bIsAlreadyLink) {
                m_xLinkPanels.Add(xNextPanel);
                int iCurrent = m_xLinkPanels.IndexOf(xNextPanel);

                Info.AddLink();
                Info.LinkInfo[iCurrent].LinkSerialNo = xNextPanel.Info.SerialNo;
                LinkRect.BackColor = Color.LightBlue;
            
                if(null != m_xParentForm1.InfoWindow) { m_xParentForm1.InfoWindow.PropertyGrid.Refresh(); }
            }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public void RemoveLinkPanel(PanelBase xDeletePanel) {
            if(null != Info.LinkInfo) {
                int iCurrent = m_xLinkPanels.IndexOf(xDeletePanel);
                if (-1 != iCurrent) { 
                    m_xLinkPanels.Remove(xDeletePanel);
                    Info.RemoveLink(iCurrent);
                }
            }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public void UpdateLinkSerial() {
            int i = 0;
            foreach (PanelBase xPanel in m_xLinkPanels) {
                Info.LinkInfo[i].LinkSerialNo = xPanel.Info.SerialNo;
                i++;
            }
        }


        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public void MovePosition(Point vMove) {
            Point vNext = new Point(Location.X + vMove.X, Location.Y + vMove.Y);
            Location = vNext;
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        private void Panel_MouseDown(object sender, MouseEventArgs e) {
            m_eOperateState = OPERTE_STATE.PUSH;
            m_vPreviousPos  = m_xParentForm1.PointToClient(System.Windows.Forms.Cursor.Position);
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        private void Panel_MouseUp(object sender, MouseEventArgs e) {
            m_eOperateState = OPERTE_STATE.NONE;
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        private void Panel_MouseMove(object sender, MouseEventArgs e) {
            bool bRefresh = false;
            
            Point vPoint = m_xParentForm1.PointToClient(System.Windows.Forms.Cursor.Position);

            if(null != m_xParentForm1.LinkSearchPanel) {
                if(m_xParentForm1.LinkSearchPanel != this) { 
                    m_xParentForm1.LinkSearchPanel.SetLinkPanel(this); 
                } 
                m_xParentForm1.ResetLinkSearchPanel();
                bRefresh                        = true;
            } else if(OPERTE_STATE.PUSH == m_eOperateState) {
                m_vPreviousPos  = vPoint;
                m_eOperateState = OPERTE_STATE.HOLD;
            } else if(OPERTE_STATE.HOLD == m_eOperateState) {
                Point vMove     = new Point(vPoint.X - m_vPreviousPos.X, vPoint.Y- m_vPreviousPos.Y);
                m_vPreviousPos  = vPoint;
                MovePosition(vMove);
                
                bRefresh = true;
            }

            if(bRefresh) { m_xParentForm1.Refresh(); }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        protected void Panel_DoubleClick(object sender, EventArgs e) {

            if (m_xParentForm1.InfoWindow.IsDisposed) {
                m_xParentForm1.CreateInfoWindow();
            }

            m_xParentForm1.InfoWindow.PropertyGrid.SelectedObject = m_xInfo;
            m_xParentForm1.InfoWindow.Show();
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        private void panel2_MouseDown(object sender, MouseEventArgs e) {
            // リンク先の検索を開始する
            m_xParentForm1.LinkSearchPanel      = this;
            LinkRect.BackColor                  = Color.Blue;
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        private void panel2_DoubleClick(object sender, EventArgs e) {
            m_xLinkPanels.Clear();
            Info.RemoveLink();
            m_xParentForm1.Refresh();
        }
    }
}
