﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace FlowLink {

    // ------------------------------------------------------------------------------------------------
    /**
     * 
     */
    static class PanelFactory {

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        static public PanelBase CreatePanel(int iPanelType, Point xPos) {
            PanelBase xPanel = null;

            if (iPanelType == (int)Info.TYPE.ACTION)            { xPanel = new ActionPanel();    }
            else if (iPanelType == (int)Info.TYPE.CONDITION)    { xPanel = new ConditionPanel(); }
            else if (iPanelType == (int)Info.TYPE.FLOW)         { xPanel = new FlowPanel();      }

            xPanel.Location = xPos;
            xPanel.Visible  = true;

            return xPanel;
        }
    }
}
