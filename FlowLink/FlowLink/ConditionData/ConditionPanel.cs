﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlowLink {

    // ------------------------------------------------------------------------------------------------
    /**
     * 
     */
    public partial class ConditionPanel : PanelBase {

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public ConditionPanel() {
            InitializeComponent();
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public override void CreateInfo() {
            m_xInfo     = new ConditionInfo();
        }

    }
}
