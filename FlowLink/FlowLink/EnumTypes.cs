﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowLink {

    // ------------------------------------------------------------------------------------------------
    /**
     * 
     */
    static class EnumTypes {

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        static private ArrayList m_strEnum = new ArrayList();

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        static public ArrayList StrEnum {
            //set { m_strEnum = value; }
            get { return m_strEnum; }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        static public void SetEnumTypes() {
            m_strEnum.Add("列挙１");
            m_strEnum.Add("列挙２");
            m_strEnum.Add("列挙３");
            m_strEnum.Add("列挙４");
            m_strEnum.Add("列挙５");
            m_strEnum.Add("列挙６");
            m_strEnum.Add("列挙７");
        }
    }
}
