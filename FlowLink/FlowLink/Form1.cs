﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.IO;

namespace FlowLink {

    public partial class Form1 : Form {

        private ArrayList   m_xPanels               = new ArrayList();
        private PanelBase   m_xLinkSearchPanel      = null;
        private bool        m_bMoveFocus            = false;
        private Point       m_vPreviousPos          = new Point();
        private InfoWindow  m_xInfoWindow           = null;

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public InfoWindow InfoWindow {
            get { return m_xInfoWindow; }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public PanelBase LinkSearchPanel {
            set { m_xLinkSearchPanel = value; }
            get { return m_xLinkSearchPanel; }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public Form1() {
            InitializeComponent();
            ContextMenuStrip = null;
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public void ResetLinkSearchPanel() {
            LinkSearchPanel.LinkRect.BackColor = (LinkSearchPanel.Info.LinkInfo == null || LinkSearchPanel.Info.LinkInfo.Count() == 0) ?
                                        Color.Gray : Color.LightBlue;
            LinkSearchPanel = null;
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public void CreateInfoWindow() {
            m_xInfoWindow = new InfoWindow();
            m_xInfoWindow.Show();

            Point xPos  = this.Location;
            xPos.X      += this.Width;
            m_xInfoWindow.Location = xPos; 
        }
        
        // ------------------------------------------------------------------------------------------------
        /**
         *  
         */
	    protected override void OnPaint(PaintEventArgs e) {
		    base.OnPaint(e);
            DrawLinkLine(e);
	    }

        // ------------------------------------------------------------------------------------------------
        /**
         *  リセット
         */
        private void Reset() {
            m_xLinkSearchPanel = null;
            foreach (PanelBase xPanel in m_xPanels) { xPanel.Dispose(); }
            m_xPanels.Clear();
            m_xInfoWindow.PropertyGrid.SelectedObject = null;

            Refresh();
        }


        // ------------------------------------------------------------------------------------------------
        /**
         *  線描画 
         */
        private void DrawLinkLine(PaintEventArgs e) {
            foreach(PanelBase x in m_xPanels) {
                Point   xPoint      = x.Location;
                Size    xSize       = x.Size;
		        Point   xStart      = new Point(xPoint.X + (xSize.Width/2), xPoint.Y + (xSize.Height/2));
		        Point   xVia1       = new Point(xStart.X + 100, xStart.Y);

                if(null == x.Info.LinkInfo) { continue; }

                for(int i = 0; i < x.Info.LinkInfo.Count(); i++) {
                    int iLinkSerialNo = x.Info.LinkInfo[i].LinkSerialNo;

                    PanelBase xLinkPanel = new PanelBase();
                    foreach(PanelBase xTemp in m_xPanels) {
                        if(iLinkSerialNo == xTemp.Info.SerialNo) { xLinkPanel = xTemp; break; }
                    }
                    
                    Point   xLinkPanelPoint  = xLinkPanel.Location;
                    Size    xLinkPanelSize   = xLinkPanel.Size;

		            Point   xEnd         = new Point(xLinkPanelPoint.X, xLinkPanelPoint.Y + (xLinkPanelSize.Height/2));
		            Point   xVia2        = new Point(xEnd.X - 100, xEnd.Y);
            
		            Pen pen = new Pen(Color.White, 5);
		            pen.Width = 1;
                    e.Graphics.DrawBezier(pen, xStart, xVia1, xVia2, xEnd);
                }
            }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         *  フローリンクファイルの入力
         */
        private void InputFile(StreamReader xReader) {

            int iStep = 0;
            while (!xReader.EndOfStream) {

                string strLine = xReader.ReadLine();

                if (strLine == "Self,")     { continue; }
                if (strLine == "Links,")    { iStep = 1; continue; }

                string[]    strArray        = strLine.Split(',');

                if (0 == iStep) {
                    int iType           = int.Parse(strArray[(int)Info.SELF_DATA.TYPE]);
                    PanelBase xPanel    = CreatePanel(iType, new Point(100,100));
                    xPanel.InputSelf(strArray);
                } else {
                    int iSerialNo       = int.Parse(strArray[(int)Info.SELF_DATA.SERIAL_NO]);
                    PanelBase xPanel    = (PanelBase)m_xPanels[iSerialNo];

                    // シリアル番号部を削除する
                    List<string> xTempList = new List<string>(strArray);
                    xTempList.RemoveAt(0);
                    strArray = xTempList.ToArray();
                    xPanel.InputLinks(strArray);
                }
            }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         *  フローリンクファイルの出力
         */
        private void OutputCsv(StreamWriter xWriter) {
            xWriter.WriteLine("Self,");
            foreach(PanelBase x in m_xPanels) { x.OutputSelf(xWriter); }

            xWriter.WriteLine("Links,");
            foreach(PanelBase x in m_xPanels) { x.OutputLinks(xWriter); }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         *  フローリンクファイルの出力
         */
        private void OutputBinary(BinaryWriter xWriter) {
            xWriter.Write("Self,");
            foreach(PanelBase x in m_xPanels) { x.OutputBinarySelf(xWriter); }

            //xWriter.Write("Links,");
            //foreach(PanelBase x in m_xPanels) { x.OutputLinks(xWriter); }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         *  プロジェクトファイルの出力
         */
        private void OutputFlk(BinaryWriter xWriter) {

            // バージョン
            int iVer = 1;
            xWriter.Write(iVer);

            foreach(PanelBase x in m_xPanels) { 

                // シリアル番号
                xWriter.Write(x.Info.SerialNo);
                
                // 座標
                xWriter.Write(x.Location.X);
                xWriter.Write(x.Location.Y);
            }
        }


        // ------------------------------------------------------------------------------------------------
        /**
         *  パネルの生成
         */
        private PanelBase CreatePanel(int iType, Point xPoint) {
            PanelBase   xPanel      = PanelFactory.CreatePanel(iType, xPoint);

            xPanel.ParentForm1      = this;
            xPanel.Info.SerialNo    = m_xPanels.Count;

            m_xPanels.Add(xPanel);
            Controls.Add(xPanel);

            return xPanel;
        }

        // ------------------------------------------------------------------------------------------------
        /**
         *  パネルの生成
         */
        private void ToolStripMenuItem_Click(object sender, EventArgs e) {
            
            ToolStripMenuItem xMenu = (ToolStripMenuItem)sender;

            Info.TYPE   eType       = (Info.TYPE)Enum.Parse(typeof(Info.TYPE), xMenu.Tag.ToString());
            Point       xPoint      = PointToClient(System.Windows.Forms.Cursor.Position);
            CreatePanel((int)eType, xPoint);
        }

        // ------------------------------------------------------------------------------------------------
        /**
         *  パネル削除
         */
        private void deleteToolStripMenuItem_Click(object sender, EventArgs e) {
            Info xSelectInfo = (Info)m_xInfoWindow.PropertyGrid.SelectedObject;

            // 同じinfoを持っているパネルを検索
            PanelBase xDelete = null;
            foreach (PanelBase xPanel in m_xPanels) {
                if (xPanel.Info == xSelectInfo) { xDelete = xPanel; break; }
            }

            if (null != xDelete) {
                // 連結情報を解除する
                foreach (PanelBase xPanel in m_xPanels) {
                    xPanel.RemoveLinkPanel(xDelete);
                }
                m_xPanels.Remove(xDelete);
                xDelete.Dispose();

                // シリアル番号振りなおし
                int i = 0;
                foreach (PanelBase xPanel in m_xPanels) {
                    xPanel.Info.SerialNo = i; i++;
                }
                foreach (PanelBase xPanel in m_xPanels) {
                    xPanel.UpdateLinkSerial();
                }
            }
            
            Refresh();
        }


        // ------------------------------------------------------------------------------------------------
        /**
         *  ファイル読み込み
         */
        private void LoadCsv(Stream xStream) {
            Reset();

            StreamReader xReader = new StreamReader(xStream);
            // ファイル入力
            InputFile(xReader);
            xReader.Close();
            xStream.Close();
        }

        // ------------------------------------------------------------------------------------------------
        /**
         *  ファイル読み込み
         */
        private void LoadFlk(BinaryReader xReader) {

            // バージョン
            int iVer = xReader.ReadInt32();

            foreach(PanelBase x in m_xPanels) { 

                // シリアル番号
                int iSerial = xReader.ReadInt32();
                if (iSerial == x.Info.SerialNo) { }

                // 座標
                Point xPos = new Point(xReader.ReadInt32(), xReader.ReadInt32());
                x.Location = xPos;
            }
        }


        // ------------------------------------------------------------------------------------------------
        /**
         *  ファイル読み込み
         */
        private void loadToolStripMenuItem_Click(object sender, EventArgs e) {

            // ダイアログを作成し、場所の指定を行わせる
            OpenFileDialog xDialog = new OpenFileDialog();

            xDialog.FileName            = "";
            xDialog.InitialDirectory    = @"C:\";
            xDialog.Filter              = "フローリンクファイル(*.flk)|*.flk";
            xDialog.Title               = "読み込みデータの指定";

            if (xDialog.ShowDialog() == DialogResult.OK) {
                {
                    // Csvファイル
                    string strPath = Path.GetFullPath(xDialog.FileName);
                    strPath = strPath.Replace(".flk", ".csv");
                    Stream xStream = new FileStream(@strPath, FileMode.Open);
                    LoadCsv(xStream);
                }

                {
                    // csvと同じ場所のバイナリ（プロジェクトファイル）
                    Stream xStream = xDialog.OpenFile();
                    using (BinaryReader xBinaryReader = new BinaryReader(xStream)) {
                        LoadFlk(xBinaryReader);
                    }
                    xStream.Close();
                }
            }

            Refresh();
        }

        // ------------------------------------------------------------------------------------------------
        /**
         *  ファイル保存
         */
        private void saveToolStripMenuItem_Click(object sender, EventArgs e) {

            // セーブダイアログを作成し、場所の指定を行わせる
            SaveFileDialog xDialog = new SaveFileDialog();

            {
                xDialog.FileName = "new_file.flk";
                xDialog.InitialDirectory = @"C:\";
                xDialog.Filter = "フローリンクファイル(*.flk)|*.flk";
                xDialog.Title = "保存先の指定";
                xDialog.OverwritePrompt = true;

                if (xDialog.ShowDialog() == DialogResult.OK)
                {

                    {
                        string strPath = Path.GetFullPath(xDialog.FileName);
                        strPath = strPath.Replace(".flk", ".csv");
                        Stream xStream = new FileStream(@strPath, FileMode.Create);
                        if (null != xStream)
                        {
                            // csv出力
                            StreamWriter xWriter = new StreamWriter(xStream);
                            OutputCsv(xWriter);
                            xWriter.Close();
                            xStream.Close();
                        }

                    }

                    {
                        // 保存処理
                        Stream xStream = xDialog.OpenFile();
                        using (BinaryWriter xBinaryWriter = new BinaryWriter(xStream))
                        {
                            // 出力
                            OutputFlk(xBinaryWriter);
                        }
                        xStream.Close();
                    }
                }
            }
        }


        // ------------------------------------------------------------------------------------------------
        /**
         *  フォームロード完了コールバック
         */
        private void Form1_Load(object sender, EventArgs e) {

            // ダブルバッファ描画によって線描画の高速化を行なう
            this.SetStyle(ControlStyles.ResizeRedraw,           true);
            this.SetStyle(ControlStyles.DoubleBuffer,           true);
            this.SetStyle(ControlStyles.UserPaint,              true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint,   true);

            CreateInfoWindow();
        }

        // ------------------------------------------------------------------------------------------------
        /**
         *  
         */
        private void Form1_MouseMove(object sender, MouseEventArgs e) {

            if (null != LinkSearchPanel) { ResetLinkSearchPanel();  }

            if(m_bMoveFocus) {
                Point vPoint    = PointToClient(System.Windows.Forms.Cursor.Position);
                Point vMove     = new Point(vPoint.X - m_vPreviousPos.X, vPoint.Y - m_vPreviousPos.Y);
                m_vPreviousPos  = vPoint;

                foreach(PanelBase x in m_xPanels) {
                    x.MovePosition(vMove);
                }

                Refresh();
            }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         *  
         */
        private void Form1_MouseDown(object sender, MouseEventArgs e) {
            if(e.Button == MouseButtons.Left) {
                m_bMoveFocus    = true;
                m_vPreviousPos  = PointToClient(System.Windows.Forms.Cursor.Position);
            }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         *  
         */
        private void Form1_MouseUp(object sender, MouseEventArgs e) {
            m_bMoveFocus = false;
        }

        // ------------------------------------------------------------------------------------------------
        /**
         *  ドラッグによる読み込み開始
         */
        private void Form1_DragEnter(object sender, DragEventArgs e) {
	        if (e.Data.GetDataPresent(DataFormats.FileDrop)) {
		        e.Effect = DragDropEffects.Copy;
	        }
        }

        // ------------------------------------------------------------------------------------------------
        /**
         *  ドラッグによる読み込み開始
         */
        private void Form1_DragDrop(object sender, DragEventArgs e) {
            string[] strNames = (string[])e.Data.GetData(DataFormats.FileDrop);

            // Csvファイル
            string strPath = Path.GetFullPath(strNames[0]);
            strPath = strPath.Replace(".flk", ".csv");
            LoadCsv(new FileStream(strPath, FileMode.Open));

            // csvと同じ場所のバイナリ（プロジェクトファイル）
            FileStream xFileStream = new FileStream(@strNames[0], FileMode.Open);
            using (BinaryReader xBinaryReader = new BinaryReader(xFileStream)) {
                LoadFlk(xBinaryReader);
            }

            Refresh();
        }

        // ------------------------------------------------------------------------------------------------
        /**
         *  
         */
        private void flowLinkToolStripMenuItem_Click(object sender, EventArgs e) {
            Reset();

            // 右クリックメニュー切り替え
            ContextMenuStrip = contextMenuStrip2;
        }

        // ------------------------------------------------------------------------------------------------
        /**
         *  
         */
        private void actionLinkToolStripMenuItem_Click(object sender, EventArgs e) {
            Reset();

            // 右クリックメニュー切り替え
            ContextMenuStrip = contextMenuStrip1;
        }
    }
}
