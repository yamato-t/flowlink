﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlowLink {

    // ------------------------------------------------------------------------------------------------
    /**
     * 
     */
    public partial class ItemPanel : PanelBase {

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public ItemPanel() {
            InitializeComponent();
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        public override void CreateInfo() {
            m_xInfo     = new ItemInfo();
        }

        // ------------------------------------------------------------------------------------------------
        /**
         * 
         */
        protected override void Panel_DoubleClick(object sender, EventArgs e) {
            if(null == m_xWindow || m_xWindow.IsDisposed) {
                m_xWindow   = new InfoWindow();
            }
            m_xWindow.PropertyGrid.SelectedObject = m_xInfo;
            m_xWindow.Show();
        }
    }
}
